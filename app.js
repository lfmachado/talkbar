/*
 This file is generated and updated by Sencha Cmd. You can edit this file as
 needed for your application, but these edits will have to be merged by
 Sencha Cmd when it performs code generation tasks such as generating new
 models, controllers or views and when running "sencha app upgrade".
 
 Ideally changes to this file would be limited and most work would be done
 in other places (such as Controllers). If Sencha Cmd cannot merge your
 changes and its generated code, it will produce a "merge conflict" that you
 will need to resolve manually.
 */

Ext.application({
  name: 'Talkbar',
  requires: [
    'Ext.MessageBox'
  ],
  
  stores: [
    "ConsumersStore",
    "ConsumedStore",
    "ConsumerxConsumedStore"
  ],
  
  models: [
    "ConsumerModel",
    "ConsumedModel",
    "ConsumerxConsumedModel"
  ],
  
  controllers: 
  [
    "MainController",
    "ConsumedController",
    "ConsumerController"
  ],
  
  views: 
  [
    'MainView', 
    
    'ReportView', 
    'ReportList', 
    
    'ConsumersView',
    'ConsumersList',
    'ConsumerEdit',
    
    'ConsumedView',
    'ConsumedList',
    'ConsumedEdit'
  ],
  
  icon: {
    '57': 'resources/icons/Icon.png',
    '72': 'resources/icons/Icon~ipad.png',
    '114': 'resources/icons/Icon@2x.png',
    '144': 'resources/icons/Icon~ipad@2x.png'
  },
  isIconPrecomposed: true,
  startupImage: {
    '320x460': 'resources/startup/320x460.jpg',
    '640x920': 'resources/startup/640x920.png',
    '768x1004': 'resources/startup/768x1004.png',
    '748x1024': 'resources/startup/748x1024.png',
    '1536x2008': 'resources/startup/1536x2008.png',
    '1496x2048': 'resources/startup/1496x2048.png'
  },
  launch: function() {
    // Destroy the #appLoadingIndicator element
    Ext.fly('appLoadingIndicator').destroy();

    /* main */
    var mainView = {xtype: "mainView"};
    /* consumer */
    var consumersView = {xtype: "consumersView"};
    var consumerEdit = {xtype: "consumerEdit"};
    /* consumed */
    var consumedView = {xtype: "consumedView"};
    var consumedEdit = {xtype: "consumedEdit"};
    /* report */
    var reportView = {xtype: "reportView"};

    // Initialize the main view
    Ext.Viewport.add(
    [
      /* main */
      mainView,
      /* consumer */
      consumersView,
      consumerEdit,
      /* consumed */
      consumedView,
      consumedEdit,
      /* report */
      reportView
    ]);
  },
  onUpdated: function() {
    Ext.Msg.confirm(
    "Application Update",
    "This application has just successfully been updated to the latest version. Reload now?",
    function(buttonId) {
      if (buttonId === 'yes') {
        window.location.reload();
      }
    }
    );
  }
});
