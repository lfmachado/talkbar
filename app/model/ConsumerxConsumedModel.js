Ext.define('Talkbar.model.ConsumerxConsumedModel',
{
  extend: 'Ext.data.Model',
  config:
  {
    idProperty: 'id',
    fields: [
      //{name: 'id', type: 'int'},
      {name: 'idConsumer', type: 'string'},
      {name: 'idConsumed', type: 'string'}
    ],
    validations:
    [
      //{type: 'presence', field: 'id'},
      {type: 'presence', field: 'idConsumer', message: 'Need to have a consumer!'},
      {type: 'presence', field: 'idConsumed', message: 'Need to have a consumed!'}
    ]
  }
});


