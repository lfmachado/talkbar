Ext.define('Talkbar.model.ConsumerModel',
{
  extend: 'Ext.data.Model',
  config:
  {
      idProperty: 'id',
      fields: [
        { name: 'id', type: 'int'},
        { name: 'name', type: 'string'},
        { name: 'email', type: 'string'},
        { name: 'spend', type: 'float'}
      ],
      validations:
      [
        { type: 'presence', field: 'id'},
        { type: 'presence', field: 'name', message: 'Need to have a name!'}
      ]
  }
});


