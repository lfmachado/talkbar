Ext.define('Talkbar.model.ConsumedModel',
{
  extend: 'Ext.data.Model',
  config:
  {
      idProperty: 'id',
      fields: [
        { name: 'id', type: 'int'},
        { name: 'name', type: 'string'},
        { name: 'price', type: 'float'},
        { name: 'number', type: 'int'}
      ],
      validations:
      [
        { type: 'presence', field: 'id'},
        { type: 'presence', field: 'name', message: 'Need to have a name!'}
      ]
  }
});


