Ext.define("Talkbar.controller.MainController",
{
  extend: "Ext.app.Controller",
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  launch: function()
  {
    localStorage.setItem('globalResetConsumer', "false");
    localStorage.setItem('globalResetConsumed', "false");

    localStorage.setItem('globalCurrentConsumer', '');
    localStorage.setItem('globalCurrentConsumed', '');
    
    this.callParent();
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  init: function()
  {
    localStorage.setItem('globalResetConsumer', "false");
    localStorage.setItem('globalResetConsumed', "false");

    localStorage.setItem('globalCurrentConsumer', '');
    localStorage.setItem('globalCurrentConsumed', '');
    
    this.callParent();
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  config:
  {
    refs:
    {
      /* main reference */
      mainView: "mainView",
      /* report */
      reportView: "reportView"
    },
    control:
    {
      /* main events */
      mainView:
      {
        resetAllEvent: "onResetAllEvent",
        consumersEvent: "onConsumersEvent",
        consumedEvent: "onConsumedEvent",
        reportEvent: "onReportEvent"
      },
      reportView:
      {
        backReportEvent: "onBackReportEvent"
      }
    }
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onConsumersEvent: function()
  {
    localStorage.setItem('globalResetConsumer', 0);

    /* load store */
    var store = Ext.getStore("ConsumersStore");
    if (store)
    {
      store.load();
    }
    /* show consumers view */
    var consumerController = this.getApplication().getController("Talkbar.controller.ConsumerController");
    var consumersView = consumerController.getConsumersView();
    Ext.Viewport.animateActiveItem(
    consumersView,
    {type: "slide", direction: "right"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onConsumedEvent: function()
  {
    /* load store */
    var store = Ext.getStore("ConsumedStore");
    if (store)
    {
      store.load();
    }
    /* show consumers view */
    var consumedController = this.getApplication().getController("Talkbar.controller.ConsumedController");
    var consumedView = consumedController.getConsumedView();
    Ext.Viewport.animateActiveItem(
    consumedView,
    {type: "slide", direction: "right"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackReportEvent: function()
  {
    /* show home view */
    var mainView = this.getMainView();
    Ext.Viewport.animateActiveItem(
    mainView,
    {type: "slide", direction: "left"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onReportEvent: function()
  {
    console.log("report...");

    /* get stores */
    var storeConsumed = Ext.getStore("ConsumedStore");
    var storeConsumers = Ext.getStore("ConsumersStore");
    var storeConsumerxConsumed = Ext.getStore("ConsumerxConsumedStore");

    if (storeConsumers && storeConsumerxConsumed && storeConsumed)
    {
      storeConsumed.load();
      storeConsumers.load();
      storeConsumerxConsumed.load();

      storeConsumers.each(function(it, id)
      {
        var currentConsumer = it.data.name;
        var currentConsumerSpend = 0;

        /* initialize arrays */
        var arrayConsumed = [];
        var arrayConsumedCount = [];

        /* get list of consumed by current consumer */
        storeConsumerxConsumed.load();
        storeConsumerxConsumed.each(function(itTemp, id)
        {
          if (itTemp.data.idConsumer === currentConsumer)
            arrayConsumed.push(itTemp.data.idConsumed);
        });

        /* initialize array of counts */
        Ext.each(arrayConsumed, function(name, index)
        {
          arrayConsumedCount.push(0);
        });

        /* count consumers that consumed that item */
        Ext.each(arrayConsumed, function(name, index)
        {
          storeConsumerxConsumed.load();
          storeConsumerxConsumed.each(function(itTemp, id)
          {
            if (itTemp.data.idConsumed === name)
              arrayConsumedCount[index]++;
          });
        });

        /* calculate how much the current consumer spend */
        Ext.each(arrayConsumed, function(name, index)
        {
          storeConsumed.load();
          var consumed = storeConsumed.findRecord("name", name);
          if (consumed)
          {
            var aux = consumed.data.price * consumed.data.number;
            currentConsumerSpend += (aux / arrayConsumedCount[index]);
          }
        });

        it.data.spend = currentConsumerSpend;
        it.dirty = true;

        console.log(it.data.name + " need to pay: R$" + it.data.spend);
      });

      storeConsumers.sync();
    }

    /* show home view */
    var reportView = this.getReportView();
    Ext.Viewport.animateActiveItem(
    reportView,
    {type: "slide", direction: "right"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onResetAllEvent: function()
  {
    console.log("reset all...");

    localStorage.setItem('globalResetConsumer', "false");
    localStorage.setItem('globalResetConsumed', "false");

    localStorage.setItem('globalCurrentConsumer', '');
    localStorage.setItem('globalCurrentConsumed', '');

    var store = Ext.getStore("ConsumedStore");
    if (store)
    {
      store.load();
      store.removeAll();
      store.sync();
    }

    store = Ext.getStore("ConsumersStore");
    if (store)
    {
      store.load();
      store.removeAll();
      store.sync();
    }

    store = Ext.getStore("ConsumerxConsumedStore");
    if (store)
    {
      store.load();
      store.removeAll();
      store.sync();
    }
  }
});