Ext.define("Talkbar.controller.ConsumerController",
{
  extend: "Ext.app.Controller",
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  launch: function()
  {
    this.callParent();
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  init: function()
  {
    this.callParent();
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  config:
  {
    refs:
    {
      /* consumers reference */
      consumersView: "consumersView",
      consumerEdit: "consumerEdit"
    },
    control:
    {
      /* VIEW LIST */
      consumersView:
      {
        backConsumerEvent: "onBackConsumerEvent",
        newConsumerEvent: "onNewConsumerEvent",
        editConsumerEvent: "onEditConsumerEvent",
        editConsumerMultiSelectionEvent: "onEditConsumerMultiSelectionEvent",
        show: "onShowConsumerEvent"
      },
      /* NEW AND EDIT */
      consumerEdit:
      {
        saveConsumerEvent: "onSaveConsumerEvent",
        deleteConsumerEvent: "onDeleteConsumerEvent",
        backConsumerEvent: "onBackConsumerEditEvent"
      }
    }
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onShowConsumerEvent: function(comp, obj) {
    /* reset list selections */
    comp.resetSelections();
    
    /* get current consumed */
    var currentConsumed = localStorage.getItem('globalCurrentConsumed');
    if (currentConsumed === "")
    {
      /* enable to create new consumeer */
      var newBtn = Ext.getCmp("consumerNewBtnId");
      newBtn.setHidden(false);
      /* enable to disclosure itens list */
      var listConsumer = Ext.getCmp("consumerListId");
      listConsumer.setOnItemDisclosure(true);
      /* enable to disclosure for each itens list */
      var items = listConsumer.listItems;
      for (var i = 0, ln = items.length; i < ln; i++) 
      {
        var item = items[i].getDisclosure();
        item.show();
      }
      
      /* has no consumed, is using list by consumer */
      return;
    }
    else
    {
      /* disable to create new consumer */
      var newBtn = Ext.getCmp("consumerNewBtnId");
      newBtn.setHidden(true);
      
      /* enable to disclosure itens list */
      var listConsumer = Ext.getCmp("consumerListId");
      listConsumer.setOnItemDisclosure(false);
      /* enable to disclosure for each itens list */
      var items = listConsumer.listItems;
      for (var i = 0, ln = items.length; i < ln; i++) 
      {
        var item = items[i].getDisclosure();
        item.hide();
      }
    }
    
    
    /* array with consumer by current consumed */
    var arrayConsumer = [];
    /* load store */
    var store = Ext.getStore("ConsumerxConsumedStore");
    if (store)
    {
      /* load and filter store */
      store.load();
      store.each(function(record, id)
      {
        if (record.data.idConsumed === currentConsumed)
        {
          /* get all consumed item */
          arrayConsumes.push(record.data.idConsumes);
        }
      });
      Ext.Array.unique(arrayConsumer);
    }
    
    /* mark itens that was consumer by consumed */
    var consumersStore = Ext.getStore("ConsumersStore");
    if (consumersStore)
    {
      var idCount = 0;
      consumersStore.load();
      consumersStore.each(function(record, id)
      {
        if (Ext.Array.contains(arrayConsumer, record.data.name))
          comp.markItemList(idCount);
        idCount++;
      });
    }

    localStorage.setItem('globalResetConsumer', "false");
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackConsumerEvent: function()
  {
    /* show home view */
    var mainController = this.getApplication().getController("Talkbar.controller.MainController");
    var mainView = mainController.getMainView();
    Ext.Viewport.animateActiveItem(
    mainView,
    {type: "slide", direction: "right"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onNewConsumerEvent: function()
  {
    /* create a defalut empty consumer */
    var newConsumer = Ext.create("Talkbar.model.ConsumerModel",
    {
      id: -1,
      name: "",
      email: "",
      spend: 0
    });
    /* add new consumer to interface */
    var consumerEdit = this.getConsumerEdit();
    consumerEdit.setRecord(newConsumer);
    
    Ext.Viewport.animateActiveItem(
    consumerEdit,
    {type: "slide", direction: "left"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onEditConsumerEvent: function(list, record)
  {
    console.log(
    "LOG -> edit consumer -> " + 
    record.data.name + " " + record.data.email + " " + record.data.spend);

    var consumerEdit = this.getConsumerEdit();
    consumerEdit.setRecord(record);
    Ext.Viewport.animateActiveItem(
    consumerEdit,
    {type: "slide", direction: "left"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onEditConsumerMultiSelectionEvent: function(list, record)
  {
    var debug = localStorage.getItem('globalResetConsumer');
    if (localStorage.getItem('globalResetConsumer') === "true")
      return;

    localStorage.setItem('globalCurrentConsumer', "");
    var currentConsumed = localStorage.getItem('globalCurrentConsumed');
    var currentConsumer = record[0].data.name;
    
    if(currentConsumed === "")
      return;

    //if (localStorage.getItem('globalCurrentConsumed') === "")
    {
      /* load store */
      var store = Ext.getStore("ConsumedStore");
      if (store)
        store.load();

      localStorage.setItem('globalCurrentConsumer', record[0].data.name);

      /* show consumers view */
      var consumedView = this.getConsumedView();

      consumedView.markSelectionsBy(record);

      Ext.Viewport.animateActiveItem(
      consumedView,
      {type: "slide", direction: "right"});
    }
    //else
    {

    }
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onSaveConsumerEvent: function()
  {
    console.log("save...");

    var consumerEdit = this.getConsumerEdit();
    var currentConsumer = consumerEdit.getRecord();
    var newConsumer = consumerEdit.getValues();
    /* update the current consumer with new values */
    currentConsumer.set("name", newConsumer.name);
    currentConsumer.set("email", newConsumer.email);
    /* check if has errors */
    var errors = currentConsumer.validate();
    if (!errors.isValid())
    {
      Ext.Msg.alert(
      "Wait!",
      errors.getByField("title")[0].getMessage(),
      Ext.emptyFn);
      currentConsumer.reject();
      return;
    }
    
    console.log(
    "LOG -> edit consumer -> " + 
    currentConsumer.data.name + " " + currentConsumer.data.email + " " + currentConsumer.data.spend);
    
    /* store */
    var consumersStore = Ext.getStore("ConsumersStore");
    consumersStore.load();
    
    /* get id */
    if(currentConsumer.data.id > 0)
    {
    }
    else
    {
      currentConsumer.data.id = 1;
      consumersStore.each(function(record, id)
      {
        if(record.data.id > currentConsumer.data.id)
          currentConsumer.data.id = id;
      });
    }
    
    consumersStore.add(currentConsumer);
    consumersStore.sync();
    /* sort */
    consumersStore.sort();
    /* back to list of consumers */
    var consumerView = this.getConsumersView();
    consumerView.setRecord(newConsumer);
    Ext.Viewport.animateActiveItem(
    consumerView,
    {type: "slide", direction: "left"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onDeleteConsumerEvent: function()
  {
    console.log("delete...");

    var consumerEdit = this.getConsumerEdit();
    var currentConsumer = consumerEdit.getRecord();
    /* remove from store */
    var consumersStore = Ext.getStore("ConsumersStore");
    consumersStore.remove(currentConsumer);
    currentConsumer.reject();
    consumersStore.sync();
    /* back to list of consumers */
    var consumerView = this.getConsumersView();
    Ext.Viewport.animateActiveItem(
    consumerView,
    {type: "slide", direction: "left"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackConsumerEditEvent: function()
  {
    var consumerEdit = this.getConsumerEdit();
    var currentConsumer = consumerEdit.getRecord();
    currentConsumer.reject();

    /* back to list of consumers */
    var consumerView = this.getConsumersView();
    Ext.Viewport.animateActiveItem(
    consumerView,
    {type: "slide", direction: "left"});
  }
});

