Ext.define("Talkbar.controller.ConsumedController",
{
  extend: "Ext.app.Controller",
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  launch: function()
  {
    this.callParent();
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  init: function()
  {
    this.callParent();
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  config:
  {
    refs:
    {
      consumedView: "consumedView",
      consumedEdit: "consumedEdit"
    },
    control:
    {
      /* LIST VIEW */
      consumedView:
      {
        backConsumedEvent: "onBackConsumedEvent",
        newConsumedEvent: "onNewConsumedEvent",
        editConsumedEvent: "onEditConsumedEvent",
        editConsumedMultiSelectionEvent: "onEditConsumedMultiSelectionEvent",
        show: "onShowConsumedViewEvent"
      },
      /* NEW AND EDIT */
      consumedEdit:
      {
        saveConsumedEvent: "onSaveConsumedEvent",
        deleteConsumedEvent: "onDeleteConsumedEvent",
        backConsumedEvent: "onBackConsumedEditEvent"
      }
    }
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onShowConsumedViewEvent: function(comp, obj) 
  {
    /* reset list selections */
    comp.resetSelections();
    /* get current consumer */
    var currentConsumer = localStorage.getItem('globalCurrentConsumer');
    if (currentConsumer === "")
    {
      /* enable to create new consumed */
      var newBtn = Ext.getCmp("consumedNewBtnId");
      newBtn.setHidden(false);
      /* enable to disclosure itens list */
      var listConsumed = Ext.getCmp("consumedListId");
      listConsumed.setOnItemDisclosure(true);
      /* enable to disclosure for each itens list */
      var items = listConsumed.listItems;
      for (var i = 0, ln = items.length; i < ln; i++)
      {
        var dItem = items[i].getDisclosure();
        dItem.show();
      }

      /* has no consumer, is using list by consumed */
      return;
    }
    else
    {
      /* disable to create new consumed */
      var newBtn = Ext.getCmp("consumedNewBtnId");
      newBtn.setHidden(true);

      /* enable to disclosure itens list */
      var listConsumed = Ext.getCmp("consumedListId");
      listConsumed.setOnItemDisclosure(false);
      /* enable to disclosure for each itens list */
      var items = listConsumed.listItems;
      for (var i = 0, ln = items.length; i < ln; i++)
      {
        var dItem = items[i].getDisclosure();
        dItem.hide();
      }
    }

    /* array with consumed by current consumer */
    var arrayConsumed = [];
    /* load store */
    var store = Ext.getStore("ConsumerxConsumedStore");
    if (store)
    {
      /* load and filter store */
      store.load();
      //store.filter("idConsumer", currentConsumer);
      store.each(function(record, id)
      {
        if (record.data.idConsumer === currentConsumer)
        {
          /* get all consumed item */
          arrayConsumed.push(record.data.idConsumed);
          console.log(record.data.idConsumed);
        }
      });
      Ext.Array.unique(arrayConsumed);
    }
    /* mark itens that was consumed by consumer */
    var consumedStore = Ext.getStore("ConsumedStore");
    if (consumedStore)
    {
      var idCount = 0;
      consumedStore.load();
      consumedStore.each(function(record, id)
      {
        if (Ext.Array.contains(arrayConsumed, record.data.name))
          comp.markItemList(idCount);
        idCount++;
      });
    }

    localStorage.setItem('globalResetConsumed', "false");
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackConsumedEvent: function()
  {
    var currentConsumer = localStorage.getItem('globalCurrentConsumer');
    if (currentConsumer === "")
    {
      /* show home view */
      var mainController = this.getApplication().getController("Talkbar.controller.MainController");
      var mainView = mainController.getConsumedView();
      Ext.Viewport.animateActiveItem(
      mainView,
      {type: "slide", direction: "left"});
    }
    else
    {
      /* show consumers view */
      var consumersView = this.getConsumersView();
      Ext.Viewport.animateActiveItem(
      consumersView,
      {type: "slide", direction: "left"});
    }
    /* reset selection */
    localStorage.setItem('globalCurrentConsumer', '');
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onNewConsumedEvent: function()
  {
    console.log("new consumed...");
    /* create randow number between 0 and 100 */
    var id_randow = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
    /* create a unique id */
    var now = new Date();
    var id = (now.getTime().toString()) + id_randow;
    /* create a defalut empty consumer */
    var newConsumed = Ext.create("Talkbar.model.ConsumedModel",
    {
      id: id,
      name: "",
      price: 0.0,
      number: 1
    });
    /* add new consumer to interface */
    var consumedEdit = this.getConsumedEdit();
    consumedEdit.setRecord(newConsumed);
    Ext.Viewport.animateActiveItem(
    consumedEdit,
    {type: "slide", direction: "left"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onEditConsumedEvent: function(list, record)
  {
    console.log("edit consumer...");

    var consumedEdit = this.getConsumedEdit();
    consumedEdit.setRecord(record);
    Ext.Viewport.animateActiveItem(
    consumedEdit,
    {type: "slide", direction: "left"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onEditConsumedMultiSelectionEvent: function(list, record)
  {
    var debug = localStorage.getItem('globalResetConsumed');
    if (localStorage.getItem('globalResetConsumed') === "true")
      return;

    localStorage.setItem('globalCurrentConsumed', "");
    var currentConsumer = localStorage.getItem('globalCurrentConsumer');
    var currentConsumed = record[0].data.name;

    if (currentConsumer === "")
    {
      /* load store */
      var store = Ext.getStore("ConsumersStore");
      if (store)
        store.load();

      localStorage.setItem('globalCurrentConsumed', currentConsumed);

      /* show consumers view */
      var consumersView = this.getConsumersView();

      Ext.Viewport.animateActiveItem(
      consumersView,
      {type: "slide", direction: "right"});
    }
    else
    {
      /* load store */
      var store = Ext.getStore("ConsumerxConsumedStore");
      if (store)
      {
        store.load();
        var remove = false;
        store.each(function(it, id)
        {
          if (currentConsumer === it.data.idConsumer &&
          currentConsumed === it.data.idConsumed)
          {
            console.log(it.data.idConsumer + " " + it.data.idConsumed);
            store.remove(it);
            it.reject();
            store.sync();
            remove = true;
          }
        });

        if (remove)
          return;

        var newConsumerxConsumed =
        Ext.create("Talkbar.model.ConsumerxConsumedModel",
        {
          idConsumer: currentConsumer,
          idConsumed: currentConsumed
        });

        store.add(newConsumerxConsumed);
        store.sync();
      }
    }
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onSaveConsumedEvent: function()
  {
    console.log("save...");

    var consumedEdit = this.getConsumedEdit();
    var currentConsumed = consumedEdit.getRecord();
    var newConsumed = consumedEdit.getValues();
    /* update the current consumer with new values */
    currentConsumed.set("name", newConsumed.name);
    currentConsumed.set("price", newConsumed.price);
    currentConsumed.set("number", newConsumed.number);
    /* check if has errors */
    var errors = currentConsumed.validate();
    if (!errors.isValid())
    {
      Ext.Msg.alert(
      "Wait!",
      errors.getByField("title")[0].getMessage(),
      Ext.emptyFn);
      currentConsumed.reject();
      return;
    }
    /* store */
    var consumedStore = Ext.getStore("ConsumedStore");
    consumedStore.add(currentConsumed);
    consumedStore.sync();
    /* sort */
    consumedStore.sort();
    /* back to list of consumers */
    var consumedView = this.getConsumedView();
    consumedView.setRecord(newConsumed);
    Ext.Viewport.animateActiveItem(
    consumedView,
    {type: "slide", direction: "left"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onDeleteConsumedEvent: function()
  {
    console.log("delete...");

    var consumedEdit = this.getConsumedEdit();
    var currentConsumed = consumedEdit.getRecord();
    /* remove from store */
    var consumedStore = Ext.getStore("ConsumedStore");
    consumedStore.remove(currentConsumed);
    currentConsumed.reject();
    consumedStore.sync();
    /* back to list of consumers */
    var consumedView = this.getConsumedView();
    Ext.Viewport.animateActiveItem(
    consumedView,
    {type: "slide", direction: "left"});
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackConsumedEditEvent: function()
  {
    var consumedEdit = this.getConsumedEdit();
    var currentConsumed = consumedEdit.getRecord();
    currentConsumed.reject();

    /* back to list of consumers */
    var consumedView = this.getConsumedView();
    Ext.Viewport.animateActiveItem(
    consumedView,
    {type: "slide", direction: "left"});
  }
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
});
