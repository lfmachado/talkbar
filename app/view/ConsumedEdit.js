Ext.define("Talkbar.view.ConsumedEdit",
{
  extend: "Ext.form.Panel",
  requires: 
  [
    "Ext.form.FieldSet",
    "Ext.field.Spinner"
  ],
  
  alias: "widget.consumedEdit",
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  config:
  {
    scrollable: "vertical"
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  initialize: function () 
  {
    this.callParent(arguments);
    
    /* top toolbar */
    
    var btnBack = 
    {
      xtype: "button",
      ui: "back",
      text: "Back",
      handler: this.onBackBtn,
      scope: this
    };  
    
    var topbar =
    {
      xtype: "toolbar",
      docked: "top",
      title: "Consumed",
      items: 
      [
        btnBack,
        { xtype: "spacer"}
      ]
    };
    
    /* bottom toolbar */
    
    var btnSave = 
    {
      xtype: "button",
      ui: "save",
      iconCls: "add",
      iconMask: true,
      handler: this.onSaveBtn,
      scope: this
    };
    
    var btnDelete = 
    {
      xtype: "button",
      iconCls: "trash",
      iconMask: true,
      handler: this.onDeleteBtn,
      scope: this
    };
    
    var bottom_toolbar =
    {
      xtype: "toolbar",
      docked: "bottom",
      items: 
      [
        btnDelete,
        { xtype: "spacer"},
        btnSave
      ]
    };
    
    /* form */
    
    var formName = 
    {
      xtype: "textfield",
      name: "name",
      label: "Name",
      required: true
    };
    
    var formPrice = 
    {
      xtype: "textfield",
      name: "price",
      label: "Price unit"
    };
    
    var formNumber = 
    {
      xtype: "spinnerfield",
      name: "number",
      label: "Units",
      minValue: 1,
      stepValue: 1,
      cycle: true
    };
    
    var form = 
    {
      xtype: "fieldset",
      items: 
      [
        formName,
        formPrice,
        formNumber
      ]
    };
    
    /* dialog */
    
    this.add([
      topbar,
      form,
      bottom_toolbar
    ]);
    
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackBtn: function()
  {
    this.fireEvent("backConsumedEvent", this);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onSaveBtn: function ()
  {
    this.fireEvent("saveConsumedEvent", this);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onDeleteBtn: function ()
  {
    this.fireEvent("deleteConsumedEvent", this);
  }
});


