Ext.define("Talkbar.view.ConsumerEdit",
{
  extend: "Ext.form.Panel",
  requires: [
    "Ext.form.FieldSet",
    "Ext.field.Email"
  ],
  
  alias: "widget.consumerEdit",
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  config:
  {
    scrollable: "vertical"
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  initialize: function () 
  {
    this.callParent(arguments);
    
    /* top toolbar */
    
    var btnBack = 
    {
      xtype: "button",
      ui: "back",
      text: "Back",
      handler: this.onBackBtn,
      scope: this
    };  
    
    var topbar =
    {
      xtype: "toolbar",
      docked: "top",
      title: "Consumer",
      items: 
      [
        btnBack,
        { xtype: "spacer"}
      ]
    };
    
    /* bottom toolbar */
    
    var btnSave = 
    {
      xtype: "button",
      ui: "save",
      iconCls: "add",
      iconMask: true,
      handler: this.onSaveBtn,
      scope: this
    };
    
    var btnDelete = 
    {
      xtype: "button",
      iconCls: "trash",
      iconMask: true,
      handler: this.onDeleteBtn,
      scope: this
    };
    
    var bottom_toolbar =
    {
      xtype: "toolbar",
      docked: "bottom",
      items: 
      [
        btnDelete,
        { xtype: "spacer"},
        btnSave
      ]
    };
    
    /* form */
    
    var formName = 
    {
      xtype: "textfield",
      name: "name",
      label: "Name",
      placeHolder: "Name",
      required: true
    };
    
    var formEmail = 
    {
      xtype: "emailfield",
      name: "email",
      label: "E-mail",
      placeHolder: "me@mail.com",
      clearIcon: true
    };
    
    var form = 
    {
      xtype: "fieldset",
      items: 
      [
        formName,
        formEmail
      ]
    };
    
    /* dialog */
    
    this.add([
      topbar,
      form,
      bottom_toolbar
    ]);
    
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackBtn: function()
  {
    this.fireEvent("backConsumerEvent", this);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onSaveBtn: function ()
  {
    this.fireEvent("saveConsumerEvent", this);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onDeleteBtn: function ()
  {
    this.fireEvent("deleteConsumerEvent", this);
  }
});


