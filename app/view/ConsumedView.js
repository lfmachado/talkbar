Ext.define('Talkbar.view.ConsumedView', {
  extend: 'Ext.Container',
  alias: "widget.consumedView",
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  initialize: function()
  {
    this.callParent(arguments);

    var backBtn =
    {
      xtype: "button",
      ui: "back",
      text: "Back",
      handler: this.onBackBtn,
      scope: this
    };

    var newBtn =
    {
      id: 'consumedNewBtnId',
      xtype: "button",
      iconCls: "add",
      iconMask: true,
      ui: "action",
      handler: this.onNewBtn,
      scope: this
    };
    
    var img = {
      xtype: "image",
      baseCls: 'Consumed32x32',
      height: 32,
      width: 32
    };

    var topbar =
    {
      xtype: "toolbar",
      //title: "Consumed",
      docked: "top",
      items:
      [
        backBtn,
        {xtype: "spacer"},
        img,
        {xtype: "spacer"},
        newBtn
      ]
    };

    var list =
    {
      id: 'consumedListId',
      xtype: "consumedList",
      mode: 'MULTI',
      //onItemDisclosure: false,
      store: Ext.getStore('ConsumedStore'),
      listeners: {
        disclose: {fn: this.onConsumedListDisclose, scope: this},
        selectionchange: {fn: this.onConsumedListMultiSelection, scope: this}
      }
    };

    this.add([topbar, list]);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  markItemList: function(id)
  {
    if(isNaN(id))
    {
      /* is not a number */
      return;
    }
    
    localStorage.setItem('globalResetConsumed', "true");
    
    var list = Ext.getCmp('consumedListId');
    list.select(id, true);
    
    localStorage.setItem('globalResetConsumed', "false");
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  resetSelections: function()
  {
    localStorage.setItem('globalResetConsumed', "true");
    
    var list = Ext.getCmp('consumedListId');
    list.deselectAll();
    
    localStorage.setItem('globalResetConsumed', "false");
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onConsumedListMultiSelection: function(list, record)
  {
    this.fireEvent('editConsumedMultiSelectionEvent', this, record);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackBtn: function()
  {
    //console.log("consumers event...");
    this.fireEvent("backConsumedEvent", this);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onNewBtn: function()
  {
    //console.log("consumers event...");
    this.fireEvent("newConsumedEvent", this);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onConsumedListDisclose: function(list, record, target, index, evt, options)
  {
    //console.log("consumed event...");
    this.fireEvent('editConsumedEvent', this, record);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  config:
  {
    layout:
    {type: "fit"}
  }
});
