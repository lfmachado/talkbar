Ext.define("Talkbar.view.ReportList",
{
  extend: "Ext.dataview.List",
  alias: "widget.reportList",
  config:
  {
    loadingText: "Loading report...",
    emptyText: '</pre> <div> Empty...</div> <pre>',
    itemTpl: '</pre><div class="list-item-title">{name}</div> <div class="list-item-narrative"> Spend: R${spend}</div><pre>'
  }
});


