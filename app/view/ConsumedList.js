Ext.define("Talkbar.view.ConsumedList",
{
  extend: "Ext.dataview.List",
  alias: "widget.consumedList",
  config:
  {
    loadingText: "Loading consumed...",
    emptyText: '</pre> Empty...<pre>',
    onItemDisclosure: true,
    itemTpl: '</pre><div class="list-item-title">{name} </div> <div class="list-item-narrative">{number} x R$ {price}</div> <pre>'
  }
});


