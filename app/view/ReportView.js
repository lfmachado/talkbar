Ext.define('Talkbar.view.ReportView', {
  extend: 'Ext.Container',
  alias: "widget.reportView",
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  initialize: function()
  {
    this.callParent(arguments);

    var backBtn =
    {
      xtype: "button",
      ui: "back",
      text: "Back",
      handler: this.onBackBtn,
      scope: this
    };
    
    var img = {
      xtype: "image",
      baseCls: 'Report32x32',
      height: 32,
      width: 32
    };

    var topbar =
    {
      xtype: "toolbar",
      //title: "Report",
      docked: "top",
      items:
      [
        backBtn,
        {xtype: "spacer"},
        img,
        {xtype: "spacer"}
      ]
    };

    var list =
    {
      xtype: "reportList",
      store: Ext.getStore('ConsumersStore')
    };

    this.add([topbar, list]);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackBtn: function()
  {
    //console.log("consumers event...");
    this.fireEvent("backReportEvent", this);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  config:
  {
    layout:
    {type: "fit"}
  }
});
