Ext.define('Talkbar.view.MainView', {
  
  extend: 'Ext.Container',
  alias: "widget.mainView",
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  initialize: function()
  {
    this.callParent(arguments);

    var topbar =
    {
      xtype: "toolbar",
      title: "Divide",
      docked: "top"
    };

    var consumers =
    {
      xtype: "button",
      text: "Consumers",
      //ui: 'round',
      iconCls: 'Consumer32x32',
      iconMask: true,
      handler: this.onConsumersBtn,
      scope: this
    };

    var consumed =
    {
      xtype: "button",
      text: "Consumed",
      //ui: 'round',
      iconCls: 'Consumed32x32',
      iconMask: true,
      handler: this.onConsumedBtn,
      scope: this
    };

    var report =
    {
      xtype: "button",
      text: "Report",
      //ui: 'round',
      iconCls: 'Report32x32',
      iconMask: true,
      handler: this.onReportBtn,
      scope: this
    };
    
    var resetall =
    {
      xtype: "button",
      text: "Reset All",
      ui: 'round',
      iconCls: 'Clean32x32',
      iconMask: true,
      handler: this.onResetAllBtn,
      scope: this
    };

    var bts =
    {
      //align: 'start',
      //pack: 'start',
      layout: 'vbox',
      width: 200,
      
      items:
      [
        {xtype: "spacer"},
        consumers,
        {xtype: "spacer"},
        consumed,
        {xtype: "spacer"},
        report,
        {xtype: "spacer"},
        resetall,
        {xtype: "spacer"}
      ]
    };
    
    var bts_ = 
    {
      layout: 'hbox',
      items:
      [
        {xtype: "spacer"},
        bts,
        {xtype: "spacer"}
      ]
    };

    this.add([topbar, bts_]);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onConsumersBtn:function()
  {
    //console.log("consumers event...");
    this.fireEvent("consumersEvent", this);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onConsumedBtn:function()
  {
    //console.log("consumed event...");
    this.fireEvent("consumedEvent", this);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onReportBtn:function()
  {
    //console.log("report event...");
    this.fireEvent("reportEvent", this);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onResetAllBtn:function()
  {
    //console.log("reset all event...");
    this.fireEvent("resetAllEvent", this);
  },
  
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  config:
  {
    layout:
    {type: "fit"}
  }
});
