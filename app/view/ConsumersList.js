Ext.define("Talkbar.view.ConsumersList",
{
  extend: "Ext.dataview.List",
  alias: "widget.consumersList",
  config:
  {
    loadingText: "Loading consumers...",
    emptyText: '</pre> <div> Empty...</div> <pre>',
    onItemDisclosure: true,
    itemTpl: '</pre><div>{name}</div><pre>'
  }
});


