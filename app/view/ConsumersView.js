Ext.define('Talkbar.view.ConsumersView', {
  extend: 'Ext.Container',
  alias: "widget.consumersView",
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  initialize: function()
  {
    this.callParent(arguments);

    var backBtn =
    {
      xtype: "button",
      ui: "back",
      text: "Back",
      handler: this.onBackBtn,
      scope: this
    };

    var newBtn =
    {
      id: 'consumerNewBtnId',
      xtype: "button",
      //text: "Consumers",
      iconCls: "add",
      iconMask: true,
      ui: "action",
      handler: this.onNewBtn,
      scope: this
    };
    
    var img = {
      xtype: "image",
      baseCls: 'Consumer32x32',
      height: 32,
      width: 32
    };

    var topbar =
    {
      xtype: "toolbar",
      //title: "Consumers",
      docked: "top",
    
      items:
      [
        backBtn,
        {xtype: "spacer"},
        img,
        {xtype: "spacer"},
        newBtn
      ]
    };

    var consumersList =
    {
      id: "consumerListId",
      xtype: "consumersList",
      store: Ext.getStore('ConsumersStore'),
      listeners: {
        disclose: {fn: this.onConsumersListDisclose, scope: this},
        selectionchange: {fn: this.onConsumersListMultiSelection, scope: this}
      }
    };

    this.add([topbar, consumersList]);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  markItemList: function(id)
  {
    if(isNaN(id))
    {
      /* is not a number */
      return;
    }
    
    localStorage.setItem('globalResetConsumer', "true");
    
    var list = Ext.getCmp('consumerListId');
    list.select(id, true);
    
    localStorage.setItem('globalResetConsumer', "false");
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  resetSelections: function()
  {
    localStorage.setItem('globalResetConsumer', "true");
    
    var list = Ext.getCmp('consumerListId');
    list.deselectAll();
    
    localStorage.setItem('globalResetConsumer', "false");
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onBackBtn: function()
  {
    //console.log("consumers event...");
    this.fireEvent("backConsumerEvent", this);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onNewBtn: function()
  {
    //console.log("consumers event...");
    this.fireEvent("newConsumerEvent", this);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onConsumersListDisclose: function(list, record, target, index, evt, options)
  {
    //console.log("consumed event...");
    this.fireEvent('editConsumerEvent', this, record);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  onConsumersListMultiSelection: function(list, record)
  {
    this.fireEvent('editConsumerMultiSelectionEvent', this, record);
  },
  //----------------------------------------------------------------------------
  //----------------------------------------------------------------------------
  config:
  {
    layout:
    {type: "fit"}
  }
});
