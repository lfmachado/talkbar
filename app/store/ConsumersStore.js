Ext.define('Talkbar.store.ConsumersStore',
{
    extend: 'Ext.data.Store',
    requires: "Ext.data.proxy.LocalStorage",
    config:
    {
      model: 'Talkbar.model.ConsumerModel',
      proxy:
      {
        type: "localstorage",
        id: "talkbar-consumers-store"
      },
      sorters: [{ property: 'name', direction: 'CRES'}]
    }
});

