Ext.define('Talkbar.store.ConsumerxConsumedStore',
{
    extend: 'Ext.data.Store',
    requires: "Ext.data.proxy.LocalStorage",
    config:
    {
      model: 'Talkbar.model.ConsumerxConsumedModel',
      proxy:
      {
        type: "localstorage",
        id: "talkbar-consumerconsumed-store"
      }
    }
});

