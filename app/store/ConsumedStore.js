Ext.define('Talkbar.store.ConsumedStore',
{
    extend: 'Ext.data.Store',
    requires: "Ext.data.proxy.LocalStorage",
    config:
    {
      model: 'Talkbar.model.ConsumedModel',
      proxy:
      {
        type: "localstorage",
        id: "talkbar-consumed-store"
      },
      sorters: [{ property: 'name', direction: 'CRES'}]
    }
});

